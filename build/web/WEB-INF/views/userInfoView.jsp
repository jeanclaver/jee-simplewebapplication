<%-- 
    Document   : userInfoView
    Created on : Nov 30, 2022, 3:21:57 AM
    Author     : vagrant
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>informations utilisateur</title>
 </head>
 <body>

    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>

    <h3>Salut ${user.userName}</h3>

    Nom d'utilisateur: <b>${user.userName}</b>
    <br />
    Genre: ${user.gender } <br />

    <jsp:include page="_footer.jsp"></jsp:include>

 </body>
</html>
