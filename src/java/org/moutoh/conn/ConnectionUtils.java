/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.moutoh.conn;
import java.sql.Connection;
import java.sql.SQLException;
/**
 *
 * @author vagrant
 */
public class ConnectionUtils {
    public static Connection getConnection() 
			  throws ClassNotFoundException, SQLException {

		// Ici, je me connecte à l'Oracle Database.
		// (Vous pouvez également utiliser d'autre base de données).
//		return OracleConnUtils.getOracleConnection();
		
		// return OracleConnUtils.getOracleConnection();
//		 return MySQLConnUtils.getMySQLConnection();
		// return SQLServerConnUtils_JTDS.getSQLServerConnection_JTDS();
		// return SQLServerConnUtils_SQLJDBC.getSQLServerConnection_SQLJDBC();
		 return PostGresConnUtils.getPostGreSQLConnection();
	}
	
	public static void closeQuietly(Connection conn) {
		try {
			conn.close();
		} catch (Exception e) {
		}
	}

	public static void rollbackQuietly(Connection conn) {
		try {
			conn.rollback();
		} catch (Exception e) {
		}
	}
}
