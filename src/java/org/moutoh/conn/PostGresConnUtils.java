/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.moutoh.conn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author vagrant
 */
public class PostGresConnUtils {
    public static Connection getPostGreSQLConnection()
            throws ClassNotFoundException, SQLException {
        // Remarque: Modifiez les paramètres de connexion en conséquence.
        String hostName = "192.168.56.10";
        String dbName = "postgres";
        String userName = "postgres";
        String password = "postgres";
        return getPostGresConnection(hostName, dbName, userName, password);
    }
    public static Connection getPostGresConnection(String hostName, String dbName,
         String userName, String password) throws SQLException,
         ClassNotFoundException {
   
     Class.forName("org.postgresql.Driver");
 
     // La structure de URL Connection pour POSTGRESQL:
     // Exemple:
     // jdbc:mysql://localhost:5432/simplehr
     String connectionURL = "jdbc:postgresql://" + hostName + ":5432/" + dbName;
 
     Connection conn = DriverManager.getConnection(connectionURL, userName,
             password);
     return conn;
 }
    
}
