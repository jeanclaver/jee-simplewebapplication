/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.moutoh.beans;

/**
 *
 * @author vagrant
 */
public class UserAccount {
    private static String GENDER_MALE ="M";
   private static String GENDER_FEMALE = "F";
   
   private String userName;
   private String gender;
   private String password;

    public UserAccount() {
    }

    public UserAccount(String userName, String gender, String password) {
        this.userName = userName;
        this.gender = gender;
        this.password = password;
    }

    /**
     * @return the GENDER_MALE
     */
    public static String getGENDER_MALE() {
        return GENDER_MALE;
    }

    /**
     * @param aGENDER_MALE the GENDER_MALE to set
     */
    public static void setGENDER_MALE(String aGENDER_MALE) {
        GENDER_MALE = aGENDER_MALE;
    }

    /**
     * @return the GENDER_FEMALE
     */
    public static String getGENDER_FEMALE() {
        return GENDER_FEMALE;
    }

    /**
     * @param aGENDER_FEMALE the GENDER_FEMALE to set
     */
    public static void setGENDER_FEMALE(String aGENDER_FEMALE) {
        GENDER_FEMALE = aGENDER_FEMALE;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
   
}
