/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.moutoh.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.moutoh.beans.UserAccount;
import org.moutoh.utils.MyUtils;

/**
 *
 * @author vagrant
 */
@WebServlet(urlPatterns = { "/userInfo" })
public class UserInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UserInfoServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		// Vérifiez si l'utilisateur s'est connecté (login) ou pas.
		UserAccount loginedUser = MyUtils.getLoginedUser(session);

		// Pas connecté (login).
		if (loginedUser == null) {
			// Redirect (Réorenter) vers la page de connexion.
			response.sendRedirect(request.getContextPath() + "/login");
			return;
		}
		// Enregistrez des informations à l'attribut de la demande avant de forward (transmettre).
		request.setAttribute("user", loginedUser);

		// Si l'utilisateur s'est connecté, forward (transmettre) vers la page
		// /WEB-INF/views/userInfoView.jsp
		RequestDispatcher dispatcher //
				= this.getServletContext().getRequestDispatcher("/WEB-INF/views/userInfoView.jsp");
		dispatcher.forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}