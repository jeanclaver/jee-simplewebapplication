<%-- 
    Document   : _header
    Created on : Nov 30, 2022, 3:10:36 AM
    Author     : vagrant
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<div style="background: #E0E0E0; height: 55px; padding: 5px;">
  <div style="float: left">
     <h1>Mon  Site</h1>
  </div>

  <div style="float: right; padding: 10px; text-align: right;">

    <!--  Sauvegarder l'utilisateurs en session avec l'attribut : loginedUser -->
     Salut <b>${loginedUser.userName}</b>
   <br/>
     Rechercher <input name="search">

  </div>

</div>
