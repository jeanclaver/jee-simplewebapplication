<%-- 
    Document   : _menu
    Created on : Nov 30, 2022, 3:11:29 AM
    Author     : vagrant
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
   
<div style="padding: 5px;">

   <a href="${pageContext.request.contextPath}/">Accueil</a>
   |
   <a href="${pageContext.request.contextPath}/productList">Liste de produits</a>
   |
   <a href="${pageContext.request.contextPath}/userInfo">Mes informations de compte</a>
   |
   <a href="${pageContext.request.contextPath}/login">Login</a>
   
</div>  