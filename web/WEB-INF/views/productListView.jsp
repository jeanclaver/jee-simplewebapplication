<%-- 
    Document   : productListView
    Created on : Nov 30, 2022, 3:23:33 AM
    Author     : vagrant
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>Liste de produits</title>
 </head>
 <body>

    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>

    <h3>Liste de produits</h3>

    <p style="color: red;">${errorString}</p>

    <table border="1" cellpadding="5" cellspacing="1" >
       <tr>
          <th>Code</th>
          <th>Nom</th>
          <th>Prix</th>
          <th>Modifier</th>
          <th>Supprimer</th>
       </tr>
       <c:forEach items="${productList}" var="product" >
          <tr>
             <td>${product.code}</td>
             <td>${product.name}</td>
             <td>${product.price}</td>
             <td>
                <a href="editProduct?code=${product.code}">Modifier</a>
             </td>
             <td>
                <a href="deleteProduct?code=${product.code}">Supprimer</a>
             </td>
          </tr>
       </c:forEach>
    </table>

    <a href="createProduct" >Créer un produit</a>

    <jsp:include page="_footer.jsp"></jsp:include>

 </body>
</html>