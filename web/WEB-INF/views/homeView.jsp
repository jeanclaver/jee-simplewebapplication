<%-- 
    Document   : homeView
    Created on : Nov 30, 2022, 3:15:48 AM
    Author     : vagrant
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
     <meta charset="UTF-8">
     <title>Page d'accueil</title>
  </head>
  <body>

     <jsp:include page="_header.jsp"></jsp:include>
     <jsp:include page="_menu.jsp"></jsp:include>
   
      <h3>Page d'accueil</h3>
     
      Ceci est une application Web simple de démonstration utilisant jsp, servlet &amp; Jdbc. <br><br>
      <b>Il comprend les fonctions suivantes:</b>
      <ul>
         <li>Login</li>
         <li>Stockage des informations utilisateur dans les cookies</li>
         <li>Liste de produits</li>
         <li>Créer un produit</li>
         <li>Modifier le produitt</li>
         <li>Supprimer le produit</li>
      </ul>

     <jsp:include page="_footer.jsp"></jsp:include>

  </body>
</html>