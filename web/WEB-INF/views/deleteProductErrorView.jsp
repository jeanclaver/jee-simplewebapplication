<%-- 
    Document   : deleteProductErrorView
    Created on : Nov 30, 2022, 3:29:24 AM
    Author     : vagrant
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>Supprimer le produit</title>
 </head>

 <body>

    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>
   
    <h3>Supprimer le produit</h3>
   
    <p style="color: red;">${errorString}</p>
    <a href="productList">Liste de produits</a>
   
    <jsp:include page="_footer.jsp"></jsp:include>
   
 </body>
</html>
